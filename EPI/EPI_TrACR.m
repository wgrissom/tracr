function [img,imginit,c,cost,imgsv,csv,p] = TrACR(data,p)

% correct for k-space trajectory (and EPI phase) errors and reconstruct an image
%
% data = k-space data (number of points x number of coils)
% p = parameter structure
%
% Copyright 2017, Julianna Ianni and Will Grissom, Vanderbilt University

% Do some argument processing
if ~isfield(p,'useparfor')
    p.useparfor = 0;
end
if ~isfield(p,'bls')
    p.bls = 1;
end

% initialize k-space error guess
if isfield(p,'cinit')
    c = p.cinit;
elseif isfield(p,'DCphase')
    c = zeros(size(p.eb,2)+size(p.ebp,2),1); % assume delays, phase error coeffs are stacked end-on-end
else 
    c = zeros(size(p.eb,2),1); % assume that x,y,z traj. error coeffs are stacked end-on-end
end
    
fprintf('Starting TrACR...\n')

tti=tic; % time start

% do an initial recon of the image with the nominal kspace trajectory
[G,p] = buildG(p,c);
img = imgupdate(data,p,G);
imginit=reshape(img,[p.dim,p.ncoils]);
% calculate initial cost:
cost = datacost(data,img,p,G);
fprintf('starting cost: %d ... \n', cost);

%save cost and images
csv(:,:,1) = c;
imgsv(:,:,1) = img;
keepgoing = 1;
while keepgoing %start alternation minimization loop
    
    fprintf('Working on iter # %d ...\n',length(cost))
    
    % update k-space error coefficients 
    fprintf('Updating trajectory...\n');
    [c,cnn] = cupdate(p,data,img,c);
    fprintf(' %d trajectory iterations taken...\n',cnn);
   
    if isfield(p,'DCphase')
        % update phi if applicable
        fprintf('Updating phi...\n');
        [c,cnn]=phiupdate(p,data,img,c);
        fprintf(' %d phi updates taken\n',cnn);
    end
    
    %setup FFT with new error coefficients
    [G,p]= buildG(p,c);
    
    %update image
    fprintf('Updating img...\n');
    img=imgupdate(data,p,G);
    
    % calculate cost and check exit conditions
    cost(end+1) = datacost(data,img,p,G);
    fprintf('Current cost: %d ... \n', cost(end));
    if length(cost) == p.maxiters+1
        fprintf('Exiting; max number of iterations reached.\n');
        keepgoing = 0;
    elseif isfield(p,'thresh') && (((cost(end-1)-cost(end))/cost(end)) <= p.thresh) %if cost exit threshold met
            % exit if cost isn't reduced significantly
            fprintf('Exiting; cost is no longer decreasing significantly.\n');
            keepgoing = 0;
    end
    
    % save progress and increment iter counter
    csv(:,end+1) = c;
    imgsv(:,:,end+1) = img;
    
    disp('====================================================');
    
end
img = reshape(img,[p.dim,p.ncoils]);
p.runtime = toc(tti); %in s
%if data was truncated, recon a final image with full data:
if isfield(p,'t')
    fprintf('reconstructing full-data final & init ims\n');
    p2=p;
    p2.datadims=p.t.datadims;
    p2.dim=p.t.dim;
    p2.knom=p.t.knom;
    p2.nproj=p.t.nproj;
    p2.npts=p.t.npts;
    p2.SENSEmap=p.t.SENSEmap;
    p2.wi=p.t.wi;
    [G,~] = buildG(p2,c);
    if isfield(p,'cinit');
        [Ginit,~]= buildG(p2,p.cinit);
    else
        [Ginit,~]= buildG(p2,zeros(size(c)));
    end
    img = imgupdate(p.t.data,p2,G);
    img = reshape(img,[p2.dim,p2.ncoils]);
    imginit = imgupdate(p.t.data,p2,Ginit);
    imginit = reshape(imginit,[p2.dim,p2.ncoils]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function to build the system matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [G,p] = buildG(p,c)

if isfield(p,'SegFFTs') && p.SegFFTs
    %with piecewise FFTs (Gmri_epi):    
    c(1:end/2)=c(1:end/2)*p.fov(end);%*-1;
    c(end/2+1:end)=-1*c(end/2+1:end);
    if isfield(p,'shotmask') % mask out the zero-ed out unacquired shots
        %(for accelerated reconstructions)
       G=Gmri_epi(true(p.datadims),true(p.dim),c(1:end/2),c(end/2+1:end),'1D',p.shotmask);
    else
       G=Gmri_epi(true(p.datadims),true(p.dim),c(1:end/2),c(end/2+1:end),'1D');
    end

elseif isfield(p,'useGmri') && p.useGmri 
    %with Gmri:
    
    k = p.knom + reshape(p.eb*c(1:size(p.eb,2)),size(p.knom));
    if ~isfield(p,'kspacescaler');
        p.kspacescaler=1./(p.dim./p.fov); %puts k-space on right scale for Gmri
    end
    k=k.*repmat(p.kspacescaler,size(k,1),1);
    %normalize k-space trajectory to -.5 to .5:
    G=Gmri(k,true(p.dim),'fov',p.dim);
    %scale appropriately (now matches output of Greengard NUFFT):
    G=(1./mean(p.dim))*G;
    %fold in estimated DC phase shift to Gmri object:
    if isfield(p,'ebp')
        Gp = diag_sp(exp(-1i*p.ebp*c(size(p.eb,2)+1:end)));
        G = block_fatrix_WG({Gp,G},'type','mult');
    end
    
else
    %with Greengard NUFFT:
    if isfield(p,'ebp')
        error('Not set up to do phase updates with Greengard');
    end
    k = p.knom + reshape(p.eb*c(1:size(p.eb,2)),size(p.knom));
    if ~isfield(p,'kspacescaler');
        p.kspacescaler=2*pi./(p.dim./p.fov./2)./2; %puts k-space on right scale for gg_nufft
    end
    % normalize it to -pi to pi
    k=k.*p.kspacescaler;
    % build the NUFFT (Greengard separable Gaussian kernel):
    G = gg_nufft(k,p.dim,p.Msp,p.R);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% image update function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function img = imgupdate(data,p,G)

if strcmp(p.reconalg,'SENSE')
    %reconstruct images with current k-space using SENSE:
    imginit=zeros(p.dim);
    [img,~,~,iter] = recon_SENS(data,imginit,p.niteri,p.wi,G,p.SENSEmap,p.tol);
    imdims=[p.dim p.ncoils];
    img = reshape(p.SENSEmap,imdims).*...
        repmat(img,[1 1 p.ncoils]);
    
else
    %reconstruct images with current k-space using SPIRiT:
    imdims=[p.dim p.ncoils];
    imginit=zeros(imdims);
    [img, ~,~,iter] = recon_SPIR(data,imginit,G,p.SPIRiTop,p.niteri,p.lam,p.wi,p.tol);
end
fprintf(' %d img iterations taken...\n',iter);
% collapse the image's spatial dims:
img = permute(img,[3 1 2]);img = img(:,:).';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% k-space error coefficient update function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [c,nn] = cupdate(p,data,img,c)

g = [];
thresh = 1e-6; % stopping threshold

for nn = 1:p.niterk
    
    % calculate gradient
    gold = g;
    g = kgradcalc(p,data,img,c(:,end));
    
    % calculate a search direction for k:
    if nn == 1
        dir = -g;
    else
        gamma=max(0,real(g(:)'*(g(:)-gold(:)))/real(gold(:)'*gold(:)));
        dir = -g + gamma*dir;
    end
    % calculate a step size that reduces cost for k:
    alpha = kstepcalc(p,data,img,c(:,end),dir,thresh);
    %take the step for k:
    step=alpha*dir;
    c(:,end+1)=c(:,end)+step;
    
    % exit if step is insignificant:
    if max([abs(p.eb*step(1:size(p.eb,2)))]) < thresh; break; end
    
end
c = c(:,end);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% phi error coefficient update function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [c,nn] = phiupdate(p,data,img,c)

g = [];
thresh = 1e-6; % stopping threshold

for nn = 1:p.niterk
    
    % calculate gradient
    gold = g;
    g = phigradcalc(p,data,img,c(:,end));
    
    % calculate a search direction for k:
    if nn == 1
        dir = -g;
    else
        gamma=max(0,real(g(:)'*(g(:)-gold(:)))/real(gold(:)'*gold(:)));
        dir = -g + gamma*dir;
    end
    % calculate a step size that reduces cost for phi:
     alpha=phistepcalc(p,data,img,c(:,end),dir,thresh);
    %take the step for k:
    step=alpha*dir;
    c(:,end+1)=c(:,end)+step;
    if max([abs(p.eb*step(size(p.eb,2)+1:end))]) < thresh; break; end
    
end
c = c(:,end);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate k-space basis derivatives
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dc = kgradcalc(p,data,img,c)

% p contains base k-space and error basis functions
% data contains k-space data
% img contains most recent image guess
% c contains most recent k-space error coefficients guess

% setup FFT with the newest coefficients
[G,p] = buildG(p,c);

Nc = size(data,2); %number of coils

% calculate gradient separately for each coil
dc = zeros([length(c) Nc]);
if p.useparfor
    s_eb=1:size(p.eb,2);
    parfor ii = 1:Nc % loop over Rx coils
        res = (data(:,ii) - G*img(:,ii)); %calc residual
        dc(s_eb,ii) = full(p.eb)'*[real(conj(1i*2*pi*(G*(img(:,ii).*p.x(:)))).*(p.wi.*res)); ...
            real(conj(1i*2*pi*(G*(img(:,ii).*p.y(:)))).*(p.wi.*res))];
    end
else
    for ii = 1:Nc % loop over Rx coils
        res = (data(:,ii) - G*img(:,ii)); %calc residual
        dc(1:size(p.eb,2),ii) = full(p.eb)'*[real(conj(1i*2*pi*(G*(img(:,ii).*p.x(:)))).*(p.wi.*res)); ...
            real(conj(1i*2*pi*(G*(img(:,ii).*p.y(:)))).*(p.wi.*res))];
    end
end
dc = sum(dc,2); %sum over dimensions

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate phi basis derivatives
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dc = phigradcalc(p,data,img,c)

% p contains base k-space and error basis functions
% data contains k-space data
% img contains most recent image guess
% c contains most recent phi error coefficients guess

% setup FFT with the newest coefficients
[G,p] = buildG(p,c);

Nc = size(data,2);

% calculate gradient
dc = zeros([length(c) Nc]);
if p.useparfor
    s_ebp=size(p.eb,2)+1:size(dc,1);
    parfor ii = 1:Nc % loop over Rx coils
        res = data(:,ii) - G*img(:,ii); %calc residual
        dc(s_ebp,ii) = p.ebp'*real(conj(1i*(G*(img(:,ii)))).*(p.wi.*res));
    end
else
    for ii = 1:Nc % loop over Rx coils
        res = data(:,ii) - G*img(:,ii); %calc residual
        dc(size(p.eb,2)+1:end,ii) = p.ebp'*real(conj(1i*(G*(img(:,ii)))).*(p.wi.*res));
    end
end
dc = sum(dc,2); %sum over dimensions

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% find a step size for trajectory update
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function alpha = kstepcalc(p,data,img,c,dir,thresh)

% setup FFT with the newest coefficients
[G,p] = buildG(p,c);

% calculate starting cost
cost = datacost(data,img,p,G);

% set max step size so that the maximum the traj can change in 1 update is less than 1/FOV
alphamax = min([1,1/(p.fov(end))/max(abs(p.eb*dir(1:size(p.eb,2))))]);
keepgoing = 1;
alpha = 0; % in case we never enter the loop
nsteps = 10; % # of steps from 0 to max step size to consider

while keepgoing && alphamax*max([abs(p.eb*dir(1:size(p.eb,2)))]) > thresh 
    
    [G,p] = buildG(p,c + dir*alphamax/nsteps);
    costt = datacost(data,img,p,G); %get cost w/current min step;
    
    % check whether alphamax/nsteps yields a lower cost than we currently have
    if costt < cost
        
        % if it does, go ahead and evaluate cost over current interval
        costt = [];
        if p.useparfor
            parfor ii = 1:nsteps
                % set up FFT and evaluate cost with this step size
                G = buildG(p,c + dir*ii*alphamax/nsteps);
                costt(ii) = datacost(data,img,p,G);
            end
        else
            for ii = 1:nsteps
                % set up FFT and evaluate cost with this step size
                G = buildG(p,c + dir*ii*alphamax/nsteps);
                costt(ii) = datacost(data,img,p,G);
            end
        end
        
        % find lowest cost and exit
        [~,mini] = min(costt);
        alpha = alphamax/nsteps*mini;
        keepgoing = 0;
        
    else
        
        % if it doesn't, decrease alphamax and look over smaller interval
        alphamax = alphamax/nsteps;
        
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% find a step size for phi update
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function alpha = phistepcalc(p,data,img,c,dir,thresh)
% setup FFT with the newest coefficients
G = buildG(p,c);

% calculate starting cost
cost = datacost(data,img,p,G);
% set max step size so that the maximum the phase can change in 1 update is less than
% pi/10
alphamax = min([1,pi./10./max(abs(p.ebp*dir(size(p.eb,2)+1:end)))]);
keepgoing = 1;
alpha = 0; % in case we never enter the loop
nsteps = 10; % # of steps from 0 to max step size to consider

while keepgoing && alphamax*max(abs(p.ebp*dir(size(p.eb,2)+1:end))) > thresh
    
    [G,p] = buildG(p,c + dir*alphamax/nsteps);
    costt = datacost(data,img,p,G); %get cost w/current min step;
    
    % check whether alphamax/nsteps yields a lower cost than we currently have
    if costt < cost
        
        % if it does, go ahead and evaluate cost over current interval
        costt = [];
        parfor ii = 1:nsteps
            % set up FFT and evaluate cost with this step size
            G = buildG(p,c + dir*ii*alphamax/nsteps);
            costt(ii) = datacost(data,img,p,G);
        end
        
        % find lowest cost and exit
        [~,mini] = min(costt);
        alpha = alphamax/nsteps*mini;
        keepgoing = 0;
        
    else
        
        % if it doesn't, decrease alphamax and look over smaller interval
        alphamax = alphamax/nsteps;
        
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate cost of data term
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cost = datacost(data,img,p,G)

% data fidelity term (same for SPIRiT and SENSE)
cost = 0;
if p.useparfor
    parfor ii = 1:p.ncoils
        res = data(:,ii) - double(G*img(:,ii));
        cost = cost + 1/2*real(res'*(p.wi.*res));
    end
else
    for ii = 1:p.ncoils
        res = data(:,ii) - double(G*img(:,ii));
        cost = cost + 1/2*real(res'*(p.wi.*res));
    end
end

% add on the SPIRiT regularization
if strcmp(p.reconalg,'SPIRiT')
    S=(abs(p.SPIRiTop*reshape(img,[p.dim p.ncoils])));
    cost = cost + p.lam*sum(S(:).^2);
end