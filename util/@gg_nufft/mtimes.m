function res = mtimes(a,bb)

if a.adjoint
	res = gg_Gnufft_back(a,bb);
else
    res = gg_Gnufft_forw(a,bb);
end

