% make a 1D object
M = 256;
N = 128;
[x,y] = ndgrid((-M/2:M/2-1)./M,(-N/2:N/2-1)./N);
f = double(x.^2 + y.^2 <= (2.5/8)^2) + double(x.^2 + y.^2 <= (3.5/8)^2);
f = circshift(f,[10 10])+0.1*randn(M,N); % remove all symmetry

% calculate its fft
F = fft2(fftshift(f));

% two-segment iFFT
F1 = F; F1(:,2:2:end) = 0;
F2 = F; F2(:,1:2:end) = 0;
fSeg = fftshift(ifft2(F1) + ifft2(F2));
printf('Two-segment iFFT NRMSE = %d',nrmse(f,fSeg,1));

% two-segment iFFT, no zero-padding
F12 = F(:,1:2:end);
F22 = F(:,2:2:end);
f12 = ifft2(F12)/2;f12 = [f12 f12];
f22 = ifft2(F22)/2;f22 = bsxfun(@times,f22,exp(1i*2*pi*(0:N/2-1)./(N))); %.*exp(1i*2*pi*
f22 = [f22 -f22];
fSeg2 = fftshift(f12+f22);
printf('Two-segment iFFT (no zero-padding) NRMSE = %d',nrmse(f,fSeg2,1));

% M-segment iFFT (M shots)
Nsh = 8;
FM = zeros(M,N,2*Nsh);
fM = zeros(M,N,2*Nsh);
for ii = 1:2*Nsh
    FM(:,ii:2*Nsh:end,ii) = F(:,ii:2*Nsh:end);
    fM(:,:,ii) = ifft2(FM(:,:,ii));
end
fSeg3 = fftshift(sum(fM,3));
printf('%d-shot iFFT (zero-padded) NRMSE = %d',Nsh,nrmse(f,fSeg3,1));

% M-segment iFFT (M shots), no zero-padding
FM2 = zeros(M,N/(2*Nsh),2*Nsh);
fM2 = zeros(M,N/(2*Nsh),2*Nsh);
for ii = 1:2*Nsh
    % extract data for this segment
    FM2(:,:,ii) = F(:,ii:2*Nsh:end);
    % go back to image domain, weighting same as full DFT (remove
    % all weighting for object implementation!) 
    fM2(:,:,ii) = ifft2(FM2(:,:,ii))/(2*Nsh);
    % compensate k-space shift (since all ifft's assume that data starts at
    % DC). We would also apply k-space delays and phase shifts at this step
    fM2(:,:,ii) = bsxfun(@times,fM2(:,:,ii),exp(1i*2*pi*(ii-1)*(0:N/(2*Nsh)-1)./N));
end
C = conj(dftmtx(2*Nsh)); % segment combination weights
fSeg4 = fftshift(reshape(col(reshape(fM2,[M*N/(2*Nsh) 2*Nsh])*C),[M N]));
printf('%d-shot iFFT (no zero padding) NRMSE = %d',Nsh,nrmse(f,fSeg4,1));

% M-segment FFT (M shots)
fM3 = reshape(reshape(fftshift(f),[M*N/(2*Nsh) 2*Nsh])*C',[M N/(Nsh*2) Nsh*2]);
% (apply conjugate delay and phase shifts here)
FSeg = zeros(M,N,1);
FM3 = zeros(M,N/(2*Nsh),2*Nsh);
for ii = 1:2*Nsh
    % un-compensate k-space shift
    fM3(:,:,ii) = bsxfun(@times,fM3(:,:,ii),exp(-1i*2*pi*(ii-1)*(0:N/(2*Nsh)-1)./N));
    % go back to Fourier domain
    FM3(:,:,ii) = fft2(fM3(:,:,ii));
    % interleave it with rest of k-space
    FSeg(:,ii:2*Nsh:end) = FM3(:,:,ii);
end
printf('%d-shot FFT NRMSE = %d',Nsh,nrmse(F,FSeg,1)); 

% I cheated and used ls fit to figure out what combination weights should be :P
% target = fftshift(f);
% for ii = 1:2*M
%     c(:,ii) = fM2\target((ii-1)*N/(2*M)+1:ii*N/(2*M));
% end

% Test the object 
G = Gmri_epi(true(M,N),true(M,N),zeros(2*Nsh-1,1),zeros(2*Nsh-1,1));

% Test k->image
fG = reshape(G'*col(fftshift(F)),[M N]);
printf('%d-shot Gmri_epi k->image NRMSE = %d',Nsh,nrmse(f,fG/(M*N),1)); 

% Test image->k
FG = reshape(G*f(:),[M N]);
printf('%d-shot Gmri_epi image->k NRMSE = %d',Nsh,nrmse(fftshift(F),FG,1));

% Compare to original NUFFT, with phase shifts and delays
dk = 5*randn(2*Nsh-1,1);
dphi = pi/2*randn(2*Nsh-1,1);
[kx,ky] = ndgrid(-M/2:M/2-1,-N/2:N/2-1);
phi = ones(size(kx));
for ii = 2:2*Nsh
    kx(:,ii:2*Nsh:end) = kx(:,ii:2*Nsh:end) + dk(ii-1);
    phi(:,ii:2*Nsh:end) = phi(:,ii:2*Nsh:end)*exp(1i*dphi(ii-1));
end
Gdel = Gmri_epi(true(M,N),true(M,N),dk,dphi);
Gorig = block_fatrix({diag_sp(phi(:)),Gmri([kx(:) ky(:)],true(M,N))},'type','mult'); % *

fGdel = reshape(Gdel'*col(fftshift(F)),[M N]);
fGorig = reshape(Gorig'*col(fftshift(F)),[M N]);
printf('%d-shot Gmri_epi versus Gmri k->image NRMSE = %d',Nsh,nrmse(fGorig,fGdel,1));

FGdel = reshape(Gdel*f(:),[M N]);
FGorig = reshape(Gorig*f(:),[M N]);
printf('%d-shot Gmri_epi versus Gmri image->k NRMSE = %d',Nsh,nrmse(FGorig,FGdel,1));

% zero out some lines of k-space, go image->k
Gskip = Gmri_epi(true(M,N),true(M,N),dk,dphi,... %zeros(2*Nsh-1,1),zeros(2*Nsh-1,1),...
        '2D',logical([1 0 1 0 1 0 1 0]));
tic 
for ii = 1:1000
    FGskip = reshape(Gskip*f(:),[M N]);
end
toc
tic
for ii = 1:1000
    fGskip = reshape(Gskip'*col(fftshift(F)),[M N]);
end
toc

% calculate fft in 1D; compare to 2D object
F1D = fft(fftshift(f,2),[],2);
Gskip1D = Gmri_epi(true(M,N),true(M,N),dk,dphi,... %zeros(2*Nsh-1,1),zeros(2*Nsh-1,1),...
        '1D',logical([1 0 1 0 1 0 1 0]));
tic
for ii = 1:1000
    FGskip1D = reshape(Gskip1D*f(:),[M N]);
end
toc
FGskip1D = fftshift(fft(fftshift(FGskip1D,1),[],1),1);
tic
for ii = 1:1000
    fGskip1D = reshape(Gskip1D'*col(fftshift(F1D,2)),[M N])*M;
end
toc
printf('%d-shot Gmri_epi 2D versus Gmri_epi 1D k->image NRMSE = %d',Nsh,nrmse(fGskip,fGskip1D,1));

printf('%d-shot Gmri_epi 2D versus Gmri_epi 1D image->k NRMSE = %d',Nsh,nrmse(FGskip,FGskip1D,1));





