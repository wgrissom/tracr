% make a 1D object
N = 256;
[x,y] = meshgrid((-N/2:N/2-1)./N);
f = double(x.^2 + y.^2 <= (2.5/8)^2) + double(x.^2 + y.^2 <= (3.5/8)^2);
f = circshift(f(:,N/2+1),[10 0])+0.1*randn(N,1); % remove all symmetry

% calculate its fft
F = fft(fftshift(f));

% two-segment iFFT
F1 = F; F1(2:2:end) = 0;
F2 = F; F2(1:2:end) = 0;
fSeg = fftshift(ifft(F1) + ifft(F2));
printf('Two-segment iFFT NRMSE = %d',nrmse(f,fSeg,1));

% two-segment iFFT, no zero-padding
F12 = F(1:2:end);
F22 = F(2:2:end);
f12 = ifft(F12)/2;f12 = [f12; f12];
f22 = ifft(F22)/2;f22 = exp(1i*2*pi*(0:N/2-1)'./(N)).*f22; %.*exp(1i*2*pi*
f22 = [f22;-f22];
fSeg2 = fftshift(f12+f22);
printf('Two-segment iFFT (no zero-padding) NRMSE = %d',nrmse(f,fSeg2,1));

% M-segment iFFT (M shots)
M = 4;
FM = zeros(N,2*M);
fM = zeros(N,2*M);
for ii = 1:2*M
    FM(ii:2*M:end,ii) = F(ii:2*M:end);
    fM(:,ii) = ifft(FM(:,ii));
end
fSeg3 = fftshift(sum(fM,2));
printf('%d-shot iFFT (zero-padded) NRMSE = %d',M,nrmse(f,fSeg3,1));

% M-segment iFFT (M shots), no zero-padding
M = 4;
FM2 = zeros(N/(2*M),2*M);
fM2 = zeros(N/(2*M),2*M);
for ii = 1:2*M
    % extract data for this segment
    FM2(:,ii) = F(ii:2*M:end);
    % go back to image domain, weighting same as full DFT (remove
    % all weighting for object implementation!) 
    fM2(:,ii) = ifft(FM2(:,ii))/(2*M);
    % compensate k-space shift (since all ifft's assume that data starts at
    % DC). We would also apply k-space delays and phase shifts at this step
    fM2(:,ii) = fM2(:,ii).*exp(1i*2*pi*(ii-1)*(0:N/(2*M)-1)'./N);
end
C = conj(dftmtx(2*M)); % segment combination weights
fSeg4 = fftshift(col(fM2*C));
printf('%d-shot iFFT (no zero padding) NRMSE = %d',M,nrmse(f,fSeg4,1));

% M-segment FFT (M shots)
fM3 = reshape(fftshift(f(:)),[N/(2*M) 2*M])*C';
% (apply conjugate delay and phase shifts here)
FSeg = zeros(N,1);
FM2 = zeros(N/(2*M),2*M);
for ii = 1:2*M
    % un-compensate k-space shift
    fM3(:,ii) = fM3(:,ii).*exp(-1i*2*pi*(ii-1)*(0:N/(2*M)-1)'./N);
    % go back to Fourier domain
    FM3(:,ii) = fft(fM3(:,ii));
    % interleave it with rest of k-space
    FSeg(ii:2*M:end) = FM3(:,ii);
end
printf('%d-shot FFT NRMSE = %d',M,nrmse(F,FSeg,1)); 

% I cheated and used ls fit to figure out what combination weights should be :P
% target = fftshift(f);
% for ii = 1:2*M
%     c(:,ii) = fM2\target((ii-1)*N/(2*M)+1:ii*N/(2*M));
% end





